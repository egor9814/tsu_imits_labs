#include <imits/labs.hpp>
#include <vector>
#include <cmath>
#include <iostream>
#include <list>
#include <chrono>

#include <QGuiApplication>
#include <QApplication>
#include <QMainWindow>
#include <QChart>
#include <QChartView>
#include <QBarSet>
#include <QBarSeries>
#include <QGridLayout>
#include <QLineEdit>

struct Histogram {
    static int exec(const std::list<double>& data, const double& limit);
};

double nextNormalRandomValue(const double& a, const double& sigma) {
    double normal = -6;
    for (size_t i = 0; i < 12; i++) {
        normal += Random::rand();
    }
    return a + sigma * normal;
}

void imits_labs::Program4::run() {
    currentTimeSeed();

    static const size_t N = 1000000;
    static auto M = 2.0;
    static auto D = 3.0;
    static auto H = 0.1;

    auto sigma = sqrt(D);

    std::list<double> selection;
    selection.resize(N);

    auto tripleSigmaBegin = M - 3 * sigma;
    auto tripleSigmaEnd = M + 3 * sigma;
    size_t tripleSigmaCount = 0;

    auto now = std::chrono::high_resolution_clock::now();
    for (auto& x : selection) {
        x = nextNormalRandomValue(M, sigma);
        if (tripleSigmaBegin < x && x < tripleSigmaEnd)
            tripleSigmaCount++;
    }
    auto elapsed = std::chrono::high_resolution_clock::now() - now;
    std::cout << "3sigma = " << static_cast<double>(tripleSigmaCount) / N;
    std::cout << " (elapsed time: " << std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count();
    std::cout << "ms)" << std::endl;

    now = std::chrono::high_resolution_clock::now();
    selection.sort();
    auto xmin = selection.front();
    auto xmax = selection.back();

    std::list<double> hData;
    auto x = selection.begin();
    auto end = selection.end();
    size_t square = 0;
    for (auto it = xmin + H / 2; it <= xmax + H; it += H) {
        size_t count = 0;
        while (x != end && *x < it) {
            count++;
            x++;
        }
        square += count;
        hData.push_back(static_cast<double>(count) / N);
    }
    std::cout << "square = " << static_cast<double>(square) / N << std::endl;
    elapsed = std::chrono::high_resolution_clock::now() - now;
    std::cout << "histogram data builded at " << std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count();
    std::cout << "ms" << std::endl;
    selection.clear();

    auto code = Histogram::exec(hData, 0.05);
    if (code != 0) {
        std::cout << "error in Histogram. code = " << code << std::endl;
    }
}


int Histogram::exec(const std::list<double>& data, const double& limit) {
    int argc = 0;
    QApplication app(argc, nullptr);

    QMainWindow window;
    window.setWindowTitle("Histogram");
    window.setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    window.setMinimumSize(0, 0);
    window.resize(640, 480);

    auto chart = new QtCharts::QChart();
    chart->setTheme(QtCharts::QChart::ChartThemeDark);

    for (const auto& it : data) {
        auto barSeries = new QtCharts::QBarSeries(chart);
        auto set = new QtCharts::QBarSet("");
        *set << it;
        set->setColor(QColor(Qt::GlobalColor::white));
        barSeries->append(set);
        chart->addSeries(barSeries);
    }

    chart->createDefaultAxes();
    auto maxY = *std::max_element(data.begin(), data.end());
    chart->axes(Qt::Vertical).first()->setRange(0.0, std::max(maxY, limit));
    chart->axes(Qt::Horizontal).first()->setRange(0, static_cast<uint32_t>(data.size()));
    /*auto axisY = qobject_cast<QtCharts::QValueAxis*>(chart->axes(Qt::Horizontal).first());
    if (axisY) axisY->setLabelFormat("%.4f");
    auto axisX = qobject_cast<QtCharts::QValueAxis*>(chart->axes(Qt::Vertical).first());
    if (axisX) axisX->setLabelFormat(" ");*/
    chart->legend()->hide();

    auto chartView = new QtCharts::QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing, true);

    window.setCentralWidget(chartView);
    window.show();

    return QApplication::exec();
}
