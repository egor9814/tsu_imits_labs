#include <imits/random.hpp>
#include <chrono>

static int32_t y {0};

void Random::seed(int32_t value) {
    y = value;
}

void Random::currentTimeSeed() {
    auto now = std::chrono::high_resolution_clock::now();
    auto nano = std::chrono::duration_cast<std::chrono::nanoseconds>(now.time_since_epoch()).count();
    seed(nano);
}

inline double subRand() {
    static int32_t a{843314861};
    static int32_t c{453816693};
    static int32_t m2{1073741824};

    y *= a;
    y += c;

    if (y < 0) {
        y = (y + m2) + m2;
    }

    return double(y) * 0.4656613E-09;
}

double Random::rand() {
    auto res = subRand();
    while (res >= 1.0 || res <= 0.0)
        res = subRand();
    return res;
}

uint64_t Random::nextDiscreteRandomValueIndex(const double *p) {
    auto gamma = rand();
    uint64_t i = 0;
    for (double sum = 0; gamma > sum; sum += p[i++]);
    return i - 1;
}
