#include <imits/labs.hpp>
#include <iostream>
#include <cmath>
#include <vector>

/**
 * @brief random value by inverted function method
 * p(x) = (x/0.36) * exp{-(x^2) / 0.72}, x >= 0
 * F(x) = I[-inf, x]{ p(y)dy } = 1 - exp{ -(x^2) / 0.72 }
 * F^-1 (x) = sqrt( -0.72 * ln(1 - x) )
 *
 * @return random not break value
 * */
double nextRandomValue() {
    return sqrt(-0.72 * log(1 - Random::rand()));
}

void imits_labs::Program3::run() {
    currentTimeSeed();

    static const size_t N = 100000;
    std::vector<double> selection;
    selection.resize(N);

    double M = 0;
    for (size_t i = 0; i < N; i++) {
        auto value = nextRandomValue();
        selection[i] = value;
        M += value;
    }
    M /= N;

    double D = 0;
    for (size_t i = 0; i < N; i++) {
        auto value = selection[i] - M;
        D += value * value;
    }
    D /= N;

    std::cout << "N = " << N << std::endl;
    std::cout << "M = " << M << std::endl;
    std::cout << "D = " << D << std::endl;
}
