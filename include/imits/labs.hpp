#ifndef __egor9814__imits__labs_hpp__
#define __egor9814__imits__labs_hpp__

#include <imits/random.hpp>

#define array_size(ARRAY) sizeof(ARRAY) / sizeof(std::remove_all_extents_t<decltype(ARRAY)>)

namespace imits_labs {

    template <uint8_t ProgramNumber>
    struct ProgramSelector;

#define declare_program(N)\
    struct Program##N : public Random { void run(); };\
    template <> struct ProgramSelector<N> { using Program = Program##N; };\

    declare_program(1)
    declare_program(2)
    declare_program(3)
    declare_program(4)
    declare_program(5)

#undef declare_program

}

#define GET_LAB(N)\
    using LabProgram = imits_labs::ProgramSelector<N>::Program;\
    LabProgram

#endif //__egor9814__imits__labs_hpp__
