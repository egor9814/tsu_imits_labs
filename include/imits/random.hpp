#ifndef __egor9814__imits__random_hpp__
#define __egor9814__imits__random_hpp__

#include <cstdint>

class Random {
public:
    static void seed(int32_t value);
    static void currentTimeSeed();

    /**
     * @brief Random value from uniform distribution in range (0, 1),
     *        source: J. Forsite, M. Malcolm, K. Mouler
     * @return random value in U(0, 1)
     * */
    static double rand();


    /**
     * @brief Random index of event
     * @param p probability distribution
     * @return index of event realization
     */
    static uint64_t nextDiscreteRandomValueIndex(const double* p);
};

#endif //__egor9814__imits__random_hpp__
