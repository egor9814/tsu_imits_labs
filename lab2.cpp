#include <imits/labs.hpp>
#include <vector>
#include <iostream>

void imits_labs::Program2::run() {
    double probabilityDistribution[11]{0};
    for (size_t i = 0; i < 6; i++) {
        probabilityDistribution[i] = probabilityDistribution[10 - i] = static_cast<double>(i + 1) / 36.0;
    }

    currentTimeSeed();

    const auto N = 100000;
    ssize_t sum = 0;
    auto n = N;
    while (n--) {
        auto a = nextDiscreteRandomValueIndex(probabilityDistribution);
        auto b = nextDiscreteRandomValueIndex(probabilityDistribution);
        if (a > b) {
            sum++;
        } else if (a < b) {
            sum--;
        }
    }
    std::cout << "result based on " << N << " games = " << (sum / static_cast<double>(N)) << std::endl;
}
