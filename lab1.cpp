#include <imits/labs.hpp>
#include <iostream>
#include <cmath>
#include <vector>

#define SelectionCountBegin 1000
#define SelectionCountEnd 10000
#define SelectionCount (SelectionCountEnd + SelectionCountBegin) / 2

#define ALPHA 0.05

double X2Limit(uint64_t freeDegree) {
    static double array[] = {
             3.8,  6.0,  7.8,  9.5, 11.1,
            12.6, 14.1, 15.5, 16.9, 18.3,
            19.7, 21.0, 22.4, 23.7, 25.0,
            26.3, 27.6, 28.9, 30.1, 31.4,
            32.7, 33.9, 35.2, 36.4, 37.7,
            38.9, 40.1, 41.3, 42.6, 43.8
    };
    return array[freeDegree - 1];
}

void imits_labs::Program1::run() {
    static constexpr auto N = SelectionCount;
    static auto k = uint64_t(1 + 3.3 * std::log10(double(N)));
    static auto free = k - 1;

    std::cout << "N = " << N << std::endl;
    std::cout << "k = " << k << std::endl;
    std::cout << "free degree = " << free << std::endl;
    std::cout << "alpha = " << ALPHA << std::endl;

    currentTimeSeed(); // init random
    std::cerr << "(debug) first random value = " << rand() << std::endl;

    std::vector<double> selection; // fill selection with random values
    selection.resize(N);
    for (auto& it : selection) {
        it = rand();
    }

    std::vector<uint64_t> intervals; // divide (0, 1) on k sections and filling value's frequencies
    intervals.resize(k);
    auto step = 1.0 / k;
    double begin = 0;
    for (auto& i : intervals) {
        i = 0;
        auto end = begin + step;
        for (auto& it : selection) {
            if (begin < it && it < end)
                i++;
        }
        begin = end;
    }

    double x2 = 0; // computing X^2
    auto Nk = double(N) / k;

    for (auto& i : intervals) {
        auto y = (i - Nk);
        x2 += (y * y) / Nk;
    }

    std::cout << std::endl;
    std::cout << "X^2 = " << x2 << std::endl;
    auto limit = X2Limit(free);
    std::cout << "Limit of X^2 = " << limit << std::endl;

    std::cout << std::endl;
    if (x2 < limit) {
        std::cout << "OK!";
    } else {
        std::cout << "FAIL!";
    }
    std::cout << std::endl;
}
