#include <imits/labs.hpp>
#include <cmath>
#include <iostream>

double nextRandomValue(double a) {
    return -1.0 / a * log(1 - Random::rand());
}

void imits_labs::Program5::run() {
    currentTimeSeed();

    static const auto T = 100.0;
    static const auto mu = 5.0;
    static const auto lambda = 1.0;

    auto idleTime = 0.0;

    auto t = nextRandomValue(lambda);
    auto z = nextRandomValue(mu);

    while (t < T) {
        auto currentTime = t;
        auto tao = nextRandomValue(lambda);
        t += tao;
        if (t > T) {
            t = T;
            tao = t - currentTime;
        }

        if (z > tao) {
            z -= tao;
        } else {
            idleTime += tao - z;
            z = nextRandomValue(mu);
        }
    }

    std::cout << "P idle = " << idleTime / T << std::endl;
}
